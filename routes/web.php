<?php
use App\Http\Controllers\LanguageController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//centagram
Route::get('/','DashboardController@dashboard');
Route::get('/signup','FrontController@signup')->name('signup');
Route::post('payment/result','Payment\PaymentController@result');
Auth::routes(['verify' => true]);

	Route::group(['prefix' => 'dashboard',], function () {
        // \Aschmelyun\Larametrics\Larametrics::routes();
			//index-dash
			Route::get('/','FrontdashController@index');
			Route::post('/','FrontController@type_post');

			//seat-select
			Route::group(['prefix' => 'seat-select'], function () {
				Route::get('/','FrontController@seat_select')->name('seat');
				Route::post('/','FrontController@seat_select_post');
			});


			//invoice-list
			Route::group(['prefix' => 'invoice'], function () {
				Route::get('/','FrontdashController@order_index');
				Route::get('/{invoice_id}','FrontdashController@invoice_index_id');
			});

			/////////////////////////admin
			//invoice-list
			Route::group(['prefix' => 'order-list'], function () {
				Route::get('/','Admindashboard@order_view');
				Route::get('/{booking_number}','Admindashboard@order_view_detail');
            });

            Route::get('scan',function(){
                return view('front.admin.scan');
            });
	});

			Route::get('/about-us','FrontdashController@about_us');
			Route::get('/terms_condition','FrontdashController@terms_condition');
			Route::get('/profile','FrontdashController@profile');
			Route::post('/profile','FrontdashController@profile_post');
			Route::get('/contact-us','FrontdashController@contact');


Route::get('logout',function(){
  Auth::logout();
  return redirect('login');
});


