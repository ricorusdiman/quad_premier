

<section>
  <div class="container" style="padding-bottom: 25vh;">
    <h1 class="header">Choose parks</h1>
    <div class="row">
      <div class="col-md-6">
        <div class="legend">
          <div class="avatar mr-1 bg-warning bg-lighten-2" style="margin-bottom: 0px !IMPORTANT;
            margin-top: 0px !IMPORTANT;">
            <span class="avatar-content"></span>
          </div>
          <span>
            Available
          </span>
          <div class="avatar mr-1 bg-warning bg-lighten-2r" style="margin-bottom: 0px !IMPORTANT;
            margin-top: 0px !IMPORTANT;background-color: #FF8C00 !important;">
            <span class="avatar-content"></span>
          </div>
          <span>
            Booking
          </span>
          <div class="avatar mr-1 bg-warning bg-lighten-2r" style="margin-bottom: 0px !IMPORTANT;
            margin-top: 0px !IMPORTANT;">
            <span class="avatar-content"></span>
          </div>
          <span>
            Reserved
          </span>
        </div>
          

        
      </div>
      <div class="col-md-6">
        <p class="text-end" style="font-family: inherit;
          text-transform: uppercase;
          letter-spacing: 0.1rem;
          margin-top: 8px;font-size: small"> <small> EVENT DATE : {{ \Carbon\Carbon::parse($project->start_date)->format('D, d M Y H:i') }} </small>
        </p>
        <h3 class="header text-end" style="color: hsl(0, 0%, 75%);">{{ $project->name }}</h3>
      </div>
    </div>
    <!--screen-->
    <div class="row">
      <div class="col-4 text-center">
        <div class="theater">
          <p>LED SCREEN</p>
          <div class="seats">
            <div class="filler"></div>
            <div class="filler"></div>
            <div class="filler"></div>
            <div class="filler"></div>
          </div>
        </div>
        <img style="width: 30%;opacity: .4" src="{{ $project->home }}" class="img img-responsive">
      </div>
      <div class="col-4">
        <div class="theater">
          <p>LED SCREEN</p>
          <p>STAGE</p>
          <div class="seats">
            <div class="filler"></div>
            <div class="filler"></div>
            <div class="filler"></div>
            <div class="filler"></div>
          </div>
        </div>
      </div>
      <div class="col-4 text-center">
        <div class="theater">
          <p>LED SCREEN</p>
          <div class="seats">
            <div class="filler"></div>
            <div class="filler"></div>
            <div class="filler"></div>
            <div class="filler"></div>
          </div>
        </div>
        <img style="width: 30%;opacity: .4" src="{{ $project->away }}" class="img img-responsive">
      </div>

    </div>
    
    <!--end screen-->
    <!--car formation-->
    <div class="row stock-images justify-content-center" style="padding-bottom: 15px">
      <div class="col-md-6 col-xs-12 row" style="flex-direction: row-reverse;transform: scaley(-1);">
        
        
        @foreach ($seat->where('type', 1) as $item)
        @php
        if($item->status == 2){
        $colors = 'darkorange';
        $disabled = 'disabled';
        }elseif($item->status == 3){
        $colors = '#c00d4c';
        $disabled = 'disabled';
        }elseif($item->status == 1){
        $colors = '#102c42';
        $disabled = '';
        }
        @endphp
        <div class="col-1 text-center dashed" style="transform: scaley(-1);">
          <input id="away{{ $item->id }}" name="layout" data-value="{{ $item->name }}" class="select_seat" type="radio" value="{{ $item->id }}" {{ $disabled }}/>
          <label for="away{{ $item->id }}">
            <div class="image" style="background-color:{{ $colors  }};transform: scaley(1);"><p class="text-white" style="padding: .5vw">{{ $item->name }}</p></div>
          </label>
        </div>
        @endforeach
        <!-- <div class="col-12" style="transform: scaley(-1);">
          <div class="theater">
            <p>Screen HOME </p>
            <div class="seats">
              <div class="filler"></div>
              <div class="filler"></div>
              <div class="filler"></div>
              <div class="filler"></div>
            </div>
          </div> -->
          <!-- </div> -->
        </div>
        <div class="col-md-6 col-xs-12 row" style="flex-direction: row-reverse;transform: scaley(-1);">
          
          @foreach ($seat->where('type', 2) as $item)
          @php
          if($item->status == 2){
          $colors = 'darkorange';
          $disabled = 'disabled';
          }elseif($item->status == 3){
          $colors = '#c00d4c';
          $disabled = 'disabled';
          }elseif($item->status == 1){
          $colors = '#102c42';
          $disabled = '';
          }
          @endphp
          @if($item->name == 'FOH')
          <div class="col-2 text-center" style="transform: scaley(-1);">
            <input id="away{{ $item->id }}" name="layout" data-value="{{ $item->name }}" class="select_seat" type="radio" value="{{ $item->id }}" disabled/>
            <label for="away{{ $item->id }}">
              <div class="image" style="background-color:color:grey;width: 4.5vw;"><p class="text-white" style="padding: .5vw">{{ $item->name }}</p></div>
            </label>
          </div>
          @else
          <div class="col-1 text-center dashed" style="transform: scaley(-1);">
            <input id="away{{ $item->id }}" name="layout" data-value="{{ $item->name }}" class="select_seat" type="radio" value="{{ $item->id }}" {{ $disabled }}/>
            <label for="away{{ $item->id }}">
              <div class="image" style="background-color:{{ $colors  }};transform: scaley(1);"><p class="text-white" style="padding: .5vw">{{ $item->name }}</p></div>
            </label>
          </div>
          @endif
          @endforeach
          <!--  <div class="col-12" style="transform: scaley(-1);">
            <div class="theater">
              <p>Screen AWAY</p>
              <div class="seats">
                <div class="filler"></div>
                <div class="filler"></div>
                <div class="filler"></div>
                <div class="filler"></div>
              </div>
            </div> -->
          </div>
          <div class="row auto-center" style="margin-top:2vh">
     

            <a id="single_image" href="{{url('images/Layout_jiexpo.jpg')}}" class="btn btn-warning auto-center" style="width: 100%">VIEW FULL LAYOUT</a>
          </div>
        </div>
        
      </div>
      <div class="footers">
        <div class="row">
          <div class="col-md-8">
            <h4 class="brand-text mb-0" style="
            font-weight: bold;
            color: white;
            font-family: inherit;
            text-transform: uppercase;
            letter-spacing: 0.1rem;
            margin-top: 8px;
            padding: 2px;
            ">Details &nbsp;&nbsp;&nbsp;&nbsp; <span id="demo" style="color: #fdac41;font-weight: lighter;">Select Park</span></h4>
          </div>
          <div class="col-md-1"></div>
          <div class="col-md-2">
            <!-- <a href="" class="btn btn-success shadow">BOOK NOW !</a> -->
            <button type="button" class="btn btn-warning shadow button-test" data-toggle="modal" data-target="#modal_passenger" style="margin: 5px;width: 80%" disabled>
            BOOK NOW !
            </button>
          </div>
        </div>
      </div>
      <!--end detail seat-->
    </div>
  </section>