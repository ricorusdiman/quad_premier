<form class="form repeater-default" action="{{url('dashboard/seat-select')}}" method="POST">
  @csrf
  <!--submit paseenger modal-->
  <div class="modal fade" id="modal_passenger" tabindex="-1" role="dialog"aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
      role="document">
      <div class="modal-content">
        <div class="modal-body" style="padding: 0 !important">
          <div class="ticket-card active">
            <div class="cover" style="margin-top: 10vh;">
              <img src="https://cdn.portalquad.com/public/images/logo/logo-book.png" alt="">

              <div class="info master-font">
                <div class="tickets-left">
                  <i class="fa fa-group"></i>Add Passenger
                </div>
              </div>
            </div>
            <div class="body">
              <!-- <p id="pasenger_count"></p> -->
              <div>
                <div class="abc">
                  <div class="row justify-content-between">
                    <div class="col-md-9 col-xs-12">
                      <div class="col-md-12 col-sm-12 col-xs-8 form-group">
                        <!-- <label for="name">Name </label> -->
                        <input type="text" class="form-control input-disabled" name="name[]" value="{{ auth()->user()->name }}" placeholder="Enter Name">
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-8 form-group">
                        <input type="number" class="form-control input-disabled" name="phone[]" value="" placeholder="Enter Your Phone Number">
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-8 form-group">
                        <input type="text" class="form-control input-disabled" name="address[]" value="" placeholder="Enter Address">
                      </div>
                    </div>
                    <!-- <div class="col-md-3 col-xs-12 auto-center" style="padding: 0">
                      <div class="col-md-12 col-sm-12 col-xs-4 form-group">
                        <button style="width: 100%" class="btn btn-danger text-nowrap px-1 down" id="remScnt" type="button"> <i
                        class="bx bx-x" ></i>
                        Delete
                        </button>
                      </div>
                    </div> -->

                  </div>
                  <hr>
                </div>
              </div>
              <div class="form-group">
                <div class="col p-0">
                  <button class="btn btn-primary update" type="button"><i class="bx bx-plus"></i>
                  Add
                  </button>
                </div>
              </div>
            </div>
            <div class="footer actions">
              <button type="button" id="testcoki" type="" data-toggle="modal" data-target="#exampleModalCenter" class="btn" style="background: #3eb1c6;">S U B M I T</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--submit invoice modal-->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"aria-hidden="true" style="background: #102c42;">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
      role="document">
      <div class="modal-content">
        <div class="modal-body" style="padding: 0 !important">
          <div class="ticket-card active">
              <div class="cover" style="margin-top: 10vh;">
              <img src="https://cdn.portalquad.com/public/images/logo/logo-book.png" alt="">

              <div class="info master-font">
                <div class="tickets-left">
                  <i class="fa fa-group"></i>EVENT DATE : {{ \Carbon\Carbon::parse($project->start_date)->format('D, d M Y H:i') }}
                </div>
              </div>
            </div>
            <div class="body">
              <div class="artist">
                <h6 class="info master-font">{{ $project->name }}</h6>
              </div>
              <div class="clearfix"></div>
              <div class="row">
                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;color:#4f4f4f;font-size:12px">
                  <tbody>
                    <tr style="font-size:12px;font-weight:bold;color:rgba(49,53,59,0.96)">
                      <td width="300" valign="middle" style="padding:16px 0 16px 10px;border-bottom:thin solid #e8e8e8">ITEM</td>
                      <td width="100" valign="middle" style="padding:16px 0;border-bottom:thin solid #e8e8e8" align="center">Qty</td>
                      <td width="200" valign="middle" style="padding:16px 10px 16px 0;border-bottom:thin solid #e8e8e8" align="right">Price</td>
                    </tr>
                    <tr>
                      <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 0 16px 10px">
                        <div><p class="name" id="ticket" style="margin: 0">SELECT YOUR PARK</p></div>
                      </td>
                      <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 0" align="center">1</td>
                      <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 10px 16px 0;font-weight:bold" id="price_parks" align="right">Rp 50,000</td>
                    </tr>
                    <tr>
                      <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 0 16px 10px">
                        <div><p class="name" id="ticket" style="margin: 0">Pasengger</p></div>
                      </td>
                      <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 0" align="center" id="counter">0</td>
                      <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 10px 16px 0;font-weight:bold" id="total_price_passenger" align="right">Rp 5,000</td>
                    </tr>
                  </tbody>
                </table>
                <!-- <hr style="border-style: dashed;width: 90%"> -->
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-4 col-sm-6 mt-75">
                  </div>
                  <div class="col-8 col-sm-6 d-flex justify-content-end mt-75">
                    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;color:rgba(49,53,59,0.96);font-size:12px">
                      <tbody>
                        <tr>
                          <td style="padding:0 0 8px" align="right">Sub Total</td>
                          <td style="padding:0 0 8px" width="150" id="sub_totals" align="right">Rp 65,000</td>
                        </tr>
                        <tr>
                          <td style="padding:0 0 8px" align="right">Tax</td>
                          <td style="padding:0 0 8px" width="150" id="taxs" align="right">Rp 6,500</td>
                        </tr>
                        <tr>
                          <td style="font-weight:bold" align="right">Total</td>
                          <td style="font-weight:bold;color:#fa591d" id="totals" align="right" width="150">Rp 71,500</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <hr>

            <div class="clearfix"></div>

            </div>
          <div class="in">
            <input type="hidden" id="input_park" data-value="" name="park">
            <input type="hidden" id="input_sub_total" data-value="" name="sub_total">
            <input type="hidden" id="input_tax" data-value="" name="tax">
            <input type="hidden" id="input_total" data-value="" name="total">
            <input type="hidden" id="price_passenger" value="{{ $project->price }}" name="price_passenger">
            <input type="hidden" id="qty_passenger" value="" name="qty_passenger">
            <input type="hidden" id="price_park" value="0" name="price_park">
            <input type="hidden" id="park_id" value="" name="seating_id">
            <input type="hidden" id="seat_number" value="{{ $seat_number }}" name="seat_qty">
            <input type="hidden" id="pessanger_type" value="{{ $type_passenger }}" name="pessanger_type">
            <input type="hidden" id="license_plat" value="{{ $license_plat }}" name="license_plat">

            <input type="hidden" id="project_id" value="{{ $project->id }}" name="project_id">
            <!-- <input type="number" id="qty_passenger" value="" name="qty_passenger"> -->

              <p class="master-font auto-center">SELECT PAYMENT METHOD</p>
              <div class="inputGroup">
                <input id="radio1" name="payment" value="qris" class="payment" type="radio"/>
                <label for="radio1">Qris<br><span style="font-size: xx-small;">Bayar dengan aplikasi pembayaran pilihan anda</span></label>
                
              </div>
            </div>
            <div class="footer">
              <button type="submit" class="btn toggle-tickets button-submit" style="background: #3eb1c6;" disabled>S U B M I T</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
