
    <!-- BEGIN: Vendor JS-->
    <script>
        var assetBaseUrl = "{{ asset('') }}";
    </script>
    <script src="https://cdn.portalquad.com/public/vendors/js/vendors.min.js
    "></script>
    <script src="https://cdn.portalquad.com/public/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="https://cdn.portalquad.com/public/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="https://cdn.portalquad.com/public/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    @yield('vendor-scripts')
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    @if($configData['mainLayoutType'] == 'vertical-menu')
    <script src="https://cdn.portalquad.com/public/js/scripts/configs/vertical-menu-light.js"></script>
    @else
    <script src="https://cdn.portalquad.com/public/js/scripts/configs/horizontal-menu.js"></script>
    @endif
    <script src="https://cdn.portalquad.com/public/js/core/app-menu.js"></script>
    <script src="https://cdn.portalquad.com/public/js/core/app.js"></script>
    <script src="https://cdn.portalquad.com/public/js/scripts/components.js"></script>
    <script src="https://cdn.portalquad.com/public/js/scripts/footer.js"></script>
    <script src="https://cdn.portalquad.com/public/js/scripts/customizer.js"></script>
    <script src="https://cdn.portalquad.com/public/assets/js/scripts.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    @yield('page-scripts')
    <!-- END: Page JS-->

