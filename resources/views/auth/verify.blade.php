@extends('layouts.fullLayoutMaster')
{{-- page title --}}
@section('title','Coming Soon')

{{-- page scripts --}}
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/coming-soon.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('vendors/css/custom-ext.css')}}">
@endsection

@section('content')
<!-- coming soon start -->
<section>
  <div class="row flexbox-container">
        <div class="col-md-12 d-lg-block d-none p-2 auto-center" style="margin-bottom: -10vh;">
      <img class="img-fluid cs-effect cs-effect-bounce" src="{{asset('images/pages/comingsoon.png')}}" alt="coming soon" width="400">
    </div>
    <div class="col-md-12 col-12 text-center p-3">
      <h1 class="error-title mb-2 master-font">{{ __('Verify Your Email Address') }}</h1>
       @if (session('resent'))
            <div class="alert alert-warning" role="alert">
              {{ __('A fresh verification link has been sent to your email address.') }}
            </div>
          @endif

           {{ __('Before proceeding, please check your email for a verification link.') }}
           <br>
          {{ __('If you did not receive the email') }},
          <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
            @csrf
            <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
          </form>
      
    </div>

  </div>
</section>
<!--/ coming soon end -->
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')
<script src="{{asset('vendors/js/coming-soon/jquery.countdown.min.js')}}"></script>
@endsection
{{-- page scripts --}}
@section('page-scripts')
<script src="{{asset('js/scripts/pages/coming-soon.js')}}"></script>
@endsection
