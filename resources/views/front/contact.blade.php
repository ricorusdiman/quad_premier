@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title', 'templateTitle')
{{-- page style --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/custom-ext.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/extensions/swiper.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/tables/datatable/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/plugins/extensions/swiper.css">
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/faq.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/pages/app-invoice.css">
@section('content')
<style>

.container {
background: #fff;
margin: 0 auto;
box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-moz-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-webkit-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-o-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-ms-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
border-radius: 20px;
-moz-border-radius: 20px;
-webkit-border-radius: 20px;
-o-border-radius: 20px;
-ms-border-radius: 20px;
}
.content-body{
background: #f8f8f8 !Important;
}
.card {
box-shadow: none !important;
}
.header {
font-weight: bold;
color: #4a4242;
font-family: inherit;
text-transform: uppercase;
letter-spacing: 0.1rem;
margin-top: 8px;
}
.faq .wrapper-content.active {
min-height: 65vh;
max-height: 65vh;
}
.horizontal-menu .header-navbar {
border-bottom: 1px solid #102c42;
}
.bg-primary {
background-color: #102c42 !important;
}
.header-navbar[class*=bg-] .navbar-nav .nav-item > a i, .header-navbar[class*=bg-] .navbar-nav .nav-item > a span {
color: white !important;
}
.content-body {
background: transparent !Important;
}
</style>
<!-- faq start -->
<section class="faq" style="background: transparent;">
  <div class="row">
    <div class="col-12">
      <div class="card faq-bg bg-transparent box-shadow-0 p-1 p-md-5">
        <div class="card-content">
          <div class="card-body p-0">
            <!-- <h1 class="faq-title text-center mb-3 master-font text-white">Contact Us</h1> -->
            
            <div class="container" style="background: none">
              <div class="row">
                <div class="col-lg-6 col-sm-12">
                  <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                    <div class="card-header pb-1">
                      <div class="card-title">
                        <h4 class="text-center header">Contact Us</h4>
                      </div>
                    </div>
                    <div class="text-center">
                      <p> <small> Please enter your details</small>
                      </p>
                    </div>
                    <div class="card-content">
                      <div class="">
                        <form method="POST" action="#">
                          @csrf
                          
                          <div class="form-group mb-50">
                            <label class="text-bold-600" for="email">Phone Number</label>
                            <input id="phone" type="number" class="form-control @error('email') is-invalid @enderror" name="phone" value="{{ auth()->user()->phone_no }}" required autocomplete="email" placeholder="Phone Number">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                          </div>
                          
                          <label class="text-bold-600" for="email">Enter You Details</label>
                          
                          <fieldset class="form-group">
                            <textarea class="form-control" id="basicTextarea" rows="8" placeholder="Textarea"></textarea>
                          </fieldset>
                          <!-- <div class="form-group mb-2">
                            <label class="text-bold-600" for="password-confirm">Confirm Password</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
                          </div> -->
                          <button type="submit" class="btn btn-warning shadow position-relative w-100">SUBMIT DATA<i
                          id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                        </form>
                        <hr>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-sm-12">
                  <img src="https://cdn.portalquad.com/public/images/logo/soon-logo.png" class="d-none d-sm-block img img-responsive" style="width: 100%">
                  
                  <!-- <div class="card auto-center"> -->
                  <!-- <img src="http://127.0.0.1:8000/images/logo/jxb_alt.png" class="img img-responsive" style="width: 100%"> -->
                  <!-- <img src="{{url('images/logo/jxb_alt.png')}}" alt="avatar" height="150" width="auto" class="auto-center"> -->
                  <!-- <h5 class="master-font">ABOUT JAKARTA EXPERIENCE BOARD</h5><hr> -->
                  <!-- <p>Coming Soon</p> -->
                  <!-- </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container auto-center" style="margin-top: -5vh;background: transparent;">
     <hr>
     <p class="text-white master-font">
        For More Information : <br>RIsa Ramandha<br>General@quadportal.com
     </p>
  </div>
</section>
@endsection
{{-- vendor scripts --}}
@section('vendor-scripts')
<<script src="https://cdn.portalquad.com/public/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/extensions/swiper.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
@endsection
{{-- page scripts --}}
@section('page-scripts')
<script src="https://cdn.portalquad.com/public/js/scripts/forms/wizard-steps.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/forms/validation/form-validation.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/pages/faq.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/forms/form-repeater.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/custom-ext.js?v=1.2.8"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/pages/app-invoice.js"></script>
@endsection