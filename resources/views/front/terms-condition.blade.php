@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title', 'templateTitle')
{{-- page style --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/custom-ext.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/extensions/swiper.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/tables/datatable/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/plugins/extensions/swiper.css">
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/faq.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/pages/app-invoice.css">
@endsection
@section('content')
<style>

.container {
background: #fff;
margin: 0 auto;
box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-moz-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-webkit-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-o-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-ms-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
border-radius: 20px;
-moz-border-radius: 20px;
-webkit-border-radius: 20px;
-o-border-radius: 20px;
-ms-border-radius: 20px;
}
.content-body{
background: #f8f8f8 !Important;
}
.card {
box-shadow: none !important;
}
.header {
font-weight: bold;
color: #4a4242;
font-family: inherit;
text-transform: uppercase;
letter-spacing: 0.1rem;
margin-top: 8px;
}
.faq .wrapper-content.active {
min-height: 65vh;
max-height: 65vh;
}
.horizontal-menu .header-navbar {
border-bottom: 1px solid #102c42;
}
.bg-primary {
background-color: #102c42 !important;
}
.header-navbar[class*=bg-] .navbar-nav .nav-item > a i, .header-navbar[class*=bg-] .navbar-nav .nav-item > a span {
color: white !important;
}
.content-body {
    background: transparent !Important;
}
</style>
<!-- faq start -->
<section class="faq" style="background: transparent;">
  <div class="row">
    <div class="col-12">
      <div class="card faq-bg bg-transparent box-shadow-0 p-1 p-md-5">
        <div class="card-content">
          <div class="card-body p-0">
            <h1 class="faq-title text-center mb-3 master-font text-white">Terms & Conditions</h1>
            
            <div class="container" style="background: none">
                <p class="card-text text-center mt-3 font-medium-1 text-white">
              Please note that by booking your ticket, you are agreeing to our terms outlined in the notice. The notice may be modified or added depending on the circumstances. Be sure to check for any up-to-date information before the show to avoid any inconveniences that may interfere with your enjoyment of the event.</p>
            
            <div id="accordion-icon-wrapper3" class="collapse-icon accordion-icon-rotate" style="margin-top: 10vh;">

                <div class="accordion" id="accordionWrapar4">
                  <div class="card collapse-header">
                    <div id="heading47" class="card-header collapsed" data-toggle="collapse" role="button" data-target="#accordion73" aria-expanded="false" aria-controls="accordion73">
                      <span class="collapse-title d-flex align-items-center"><i class="bx bxs-circle font-small-1"></i>
                        All prices include 10% government tax and service fees.
                      </span>
                    </div>
                  
                  </div>
                  <div class="card collapse-header">
                    <div id="heading93" class="card-header collapsed" data-toggle="collapse" role="button" data-target="#accordion36" aria-expanded="false" aria-controls="accordion36">
                      <span class=" collapse-title d-flex align-items-center"><i class="bx bxs-circle font-small-1"></i>
                        Purchased ticket cannot be exchanged, refunded, or cancelled.
                      </span>
                    </div>

                  </div>
                  <div class="card collapse-header">
                    <div id="heading57" class="card-header" data-toggle="collapse" role="button" data-target="#accordion74" aria-expanded="false" aria-controls="accordion74">
                      <span class=" collapse-title d-flex align-items-center"><i class="bx bxs-circle font-small-1"></i>
                        Each ticket has unique QR Code and valid for 1 person only.
                      </span>
                    </div>

                  </div>
                  <div class="card collapse-header">
                    <div id="heading84" class="card-header" data-toggle="collapse" role="button" data-target="#accordion803" aria-expanded="false" aria-controls="accordion803">
                      <span class=" collapse-title d-flex align-items-center"><i class="bx bxs-circle font-small-1"></i>
                        Attendees who are behaving in a disorderly, offensive, or inappropriate manner, and refuses to obey instructions/warnings from event staff will be removed immediately. No refunds will be given.
                      </span>
                    </div>

                  </div>
                  <div class="card collapse-header">
                    <div id="heading94" class="card-header" data-toggle="collapse" role="button" data-target="#accordion122" aria-expanded="false" aria-controls="accordion122">
                      <span class=" collapse-title d-flex align-items-center"><i class="bx bxs-circle font-small-1"></i>
                        It is mandatory for attendees to wear mask and carry hand sanitizer. Vehicle (car) is recommended to have a door visor should it rains.

                      </span>
                    </div>

                  </div>
                  
                </div>
              </div>

            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>

</section>
@endsection
{{-- vendor scripts --}}
@section('vendor-scripts')
<<script src="https://cdn.portalquad.com/public/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/extensions/swiper.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
@endsection
{{-- page scripts --}}
@section('page-scripts')
<script src="https://cdn.portalquad.com/public/js/scripts/forms/wizard-steps.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/forms/validation/form-validation.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/pages/faq.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/forms/form-repeater.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/custom-ext.js?v=1.2.8"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/pages/app-invoice.js"></script>
@endsection