@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title', 'templateTitle')
{{-- page style --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/custom-ext.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/extensions/swiper.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/tables/datatable/responsive.bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/plugins/extensions/swiper.css">
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/faq.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/pages/app-invoice.css">
@endsection
@section('content')
  
<style>
html body {
background-color: #102c42 !Important;
}
.horizontal-menu .header-navbar {
border-bottom: 1px solid #102c42;
}
.bg-primary {
background-color: #102c42 !important;
}
.header-navbar[class*=bg-] .navbar-nav .nav-item > a i, .header-navbar[class*=bg-] .navbar-nav .nav-item > a span {
color: white !important;
}

 .timer {
  display: inline-block;
  font-size: .7vw;
  list-style-type: none;
  padding: 1em;
  text-transform: uppercase;
}

.timer-2{
  display: block;
  font-size: 1vw;
}

 .timer {
  display: inline-block;
  font-size: .7vw;
  list-style-type: none;
  padding: 1em;
  text-transform: uppercase;
}

.timer-2{
  display: block;
  font-size: 1vw;
}

@media only screen and (max-width: 600px) {
 .timer {
  display: inline-block;
  font-size: 5vw;
  list-style-type: none;
  padding: 1em;
  text-transform: uppercase;
}

.timer-2{
  display: block;
  font-size: 10vw;
}

</style>
    
  <!-- faq start -->


  <section class="invoice-view-wrapper">
   
    <div class="row">
    
      <div class="col-md-3 col-xs-12"></div>
          
      <div class="col-md-6 col-xs-12">
        <div class="card" style="padding: 4vh;">
          <tbody>
            <tr>
              <td style="padding:0 20px 12px;font-size:16px;font-weight:800;color:rgba(49,53,59,0.96)">
              </td>
            </tr>
            <tr>
              <td style="padding:0 20px">
                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;background-color:#f3f4f5;border-radius:12px">
                  <tbody><tr>
                    <td style="padding:14px 16px">
                      <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;color:#4f4f4f;font-size:15px">
                        <tbody>
                          <tr>
                            <td width="220" style="font-size:14px;color:rgba(49,53,59,0.96);padding:10px 0 10px 8px;vertical-align:top;line-height:1.6em"><span class="il master-font">Booking ID</span></td>
                            <td width="340" style="font-weight:bold;font-size:14px;padding:10px 0;vertical-align:top;line-height:1.6em">
                              <a href="#">{{ $order->booking_code }}</a></td>
                            </tr>
                            <tr>
                              <td width="220" style="font-size:14px;color:rgba(49,53,59,0.96);padding:10px 0 10px 8px;vertical-align:top;line-height:1.6em" class="master-font">Parking Name</td>
                              <td width="340" style="font-weight:bold;font-size:14px;padding:10px 0;vertical-align:top;line-height:1.6em">{{ $order->seat->name }}</td>
                            </tr>
                            <tr>
                              <td width="220" style="font-size:14px;color:rgba(49,53,59,0.96);padding:10px 0 10px 8px;vertical-align:top;line-height:1.6em" class="master-font">Payment Channel</td>
                              <td width="340" style="font-weight:bold;font-size:14px;padding:10px 0;vertical-align:top;line-height:1.6em">{{ $order->payment_type }}</td>
                            </tr>
                            <tr>
                              <td width="220" style="font-size:14px;color:rgba(49,53,59,0.96);padding:10px 0 10px 8px;vertical-align:top;line-height:1.6em" class="master-font">STATUS</td>
                              <td width="340" class="text-{{ $order->stat->css }}" style="font-weight:bold;font-size:14px;padding:10px 0;vertical-align:top;line-height:1.6em">{{ $order->stat->name }}</td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody></table>
                </td>
              </tr>
              <tr>
                <td style="padding:12px 0"></td>
              </tr>
              <br>
              <tr>
                <td style="padding:0 20px 0;font-size:16px;font-weight:800;color:rgba(49,53,59,0.96)" class="master-font">
                  Order Details
                </td>
              </tr>
              <tr>
                <td style="padding:10px 10px 0">
                  <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;color:#4f4f4f;font-size:12px">
                    <tbody>
                      <tr style="font-size:12px;font-weight:bold;color:rgba(49,53,59,0.96)">
                        <td width="300" valign="middle" style="padding:16px 0 16px 10px;border-bottom:thin solid #e8e8e8">Drive-in Cinema Ticket</td>

                        <td width="100" valign="middle" style="padding:16px 0;border-bottom:thin solid #e8e8e8" align="center">Qty</td>
                        <td width="200" valign="middle" style="padding:16px 10px 16px 0;border-bottom:thin solid #e8e8e8" align="right">Price</td>
                      </tr>
                      <tr style="color:rgba(49,53,59,0.96)">
                        <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 0 16px 10px">
                          <div>{{ $order->seat->name }}</div>
                        </td>

                        <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 0" align="center">1</td>
                        <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 10px 16px 0;font-weight:bold" align="right">Rp 0</td>
                      </tr>
                       <tr style="color:rgba(49,53,59,0.96)">
                        <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 0 16px 10px">
                          <div>Passenger</div>
                        </td>

                        <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 0" align="center">{{ $order->qty }}</td>
                        <td valign="top" style="border-bottom:thin solid #e8e8e8;padding:16px 10px 16px 0;font-weight:bold" align="right">Rp @curencyF($order->total-$order->fee)</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>

              <tr>
                <td style="padding:8px 0"></td>
              </tr>
              <tr>
                <td style="padding:0 20px">
                  <table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-collapse:collapse;color:rgba(49,53,59,0.96);font-size:12px">
                    <tbody>
                      <tr>
                        <td style="padding:0 0 8px" align="right">Sub Total</td>
                        <td style="padding:0 0 8px" width="150" align="right">Rp @curencyF($order->total-$order->fee)</td>
                      </tr>
                      <tr>
                        <td style="padding:0 0 8px" align="right">Tax</td>
                        <td style="padding:0 0 8px" width="150" align="right">Rp @curencyF($order->fee)</td>
                      </tr>

                      <tr>
                        <td style="font-weight:bold" align="right">Total</td>
                        <td style="font-weight:bold;color:#fa591d" align="right" width="150">Rp @curencyF($order->total)</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td style="padding:8px 0"></td>
              </tr>

              <tr>
                <td style="padding:12px 0"></td>
              </tr>
            </tbody>
          </div>

        </div>


      </div>
    <div class="container">
      <hr>
    </div>
    <div class="row">
      <h3 class="master-font text-white text-center" style="margin:auto;"><a href="{{url('/')}}" class="text-warning">HOME</a> || <a href="{{url('/')}}" class="text-warning">TERMS & CONDITION</a></h3>
    </div>
    </section>
  @endsection
  {{-- vendor scripts --}}
  @section('vendor-scripts')
  <<script src="https://cdn.portalquad.com/public/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/extensions/swiper.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
@endsection
{{-- page scripts --}}
@section('page-scripts')
<script src="https://cdn.portalquad.com/public/js/scripts/forms/wizard-steps.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/forms/validation/form-validation.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/pages/faq.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/forms/form-repeater.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/custom-ext.js?v=1.2.8"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/pages/app-invoice.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js"></script>
 
  @endsection
