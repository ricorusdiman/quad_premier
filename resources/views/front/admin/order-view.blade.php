@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title', 'templateTitle')
{{-- page style --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/custom-ext.css">

<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/extensions/swiper.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.7/css/rowReorder.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/plugins/extensions/swiper.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/pages/faq.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/pages/app-invoice.css">


@endsection
@section('content')
<style>
html body {
background-color: #102c42 !Important;
}
.container {
background: #fff;
margin: 0 auto;
box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-moz-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-webkit-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-o-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-ms-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
border-radius: 20px;
-moz-border-radius: 20px;
-webkit-border-radius: 20px;
-o-border-radius: 20px;
-ms-border-radius: 20px;
}
.card {
box-shadow: none !important;
}
.header {
font-weight: bold;
color: #4a4242;
font-family: inherit;
text-transform: uppercase;
letter-spacing: 0.1rem;
margin-top: 8px;
}
.faq .wrapper-content.active {
min-height: 65vh;
max-height: 65vh;
}
.horizontal-menu .header-navbar {
border-bottom: 1px solid #102c42;
}
.bg-primary {
background-color: #102c42 !important;
}
.header-navbar[class*=bg-] .navbar-nav .nav-item > a i, .header-navbar[class*=bg-] .navbar-nav .nav-item > a span {
color: white !important;
}
</style>
<!-- faq start -->

<section style="background: transparent;margin-top: -3.75rem !important;">
<div class="">
<h3 class="master-font auto-center text-white">All Order</h3>
</div>
  <div class="container">
  <div class="row">
    <div class="col-xs-12">
      <table summary="This table shows how to create responsive tables using Datatables' extended functionality" class="table table-bordered table-hover dt-responsive">
        
        <thead>
          <tr>
            <th>Booking ID</th>
            <th>parking_number</th>
            <th>Plat Number</th>
            <th>Payment Type</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>

        @foreach($order as $items)
          <tr>
            <td><a href="{{url('dashboard/order-list/'.$items->booking_code)}}">{{ $items->booking_code }}</a></td>
            <td>{{ $items->seat->name }}</td>
            <td>{{ $items->licens }}</td>
            <td>{{ $items->payment_type }}</td>
            <td>{{ $items->stat->name }}</td>
          </tr>
        @endforeach
        </tbody>
         
       
      </table>
    </div>
  </div>
</div>

</section>
@endsection
{{-- vendor scripts --}}
@section('vendor-scripts')
<script src="https://cdn.portalquad.com/public/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/extensions/swiper.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script>
@endsection
{{-- page scripts --}}
@section('page-scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js
"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/forms/validation/form-validation.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/pages/faq.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/forms/form-repeater.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/custom-ext.js?v=1.2.8"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/pages/app-invoice.js"></script>


<script>
$('table').DataTable();
</script>
@endsection