@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title', 'templateTitle')
{{-- page style --}}
@section('vendor-styles')
<!-- <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/custom-ext.css')}}"> -->
<!-- <script src="{{asset('vendors/js/custom-ext.js')}}"></script> -->
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/custom-ext.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/extensions/swiper.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/tables/datatable/datatables.min.css">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/plugins/extensions/swiper.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/pages/faq.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/css/pages/app-invoice.css">
@endsection
@section('content')
<style>
.container {
width: 900px;
background: #fff;
margin: 0 auto;
box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-moz-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-webkit-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-o-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
-ms-box-shadow: 0px 15px 16.83px 0.17px rgba(0, 0, 0, 0.05);
border-radius: 20px;
-moz-border-radius: 20px;
-webkit-border-radius: 20px;
-o-border-radius: 20px;
-ms-border-radius: 20px;
}
.content-body{
background: transparent; !Important;
}
.card {
box-shadow: none !important;
}
.header {
font-weight: bold;
color: #4a4242;
font-family: inherit;
text-transform: uppercase;
letter-spacing: 0.1rem;
margin-top: 8px;
}
.faq .wrapper-content.active {
min-height: 65vh;
max-height: 65vh;
}
.horizontal-menu .header-navbar {
border-bottom: 1px solid #102c42;
}
.bg-primary {
background-color: transparent; !important;
}
.header-navbar[class*=bg-] .navbar-nav .nav-item > a i, .header-navbar[class*=bg-] .navbar-nav .nav-item > a span {
color: white !important;
}
</style>
<!-- faq start -->
<section class="faq" style="background-color: transparent;">
  <div class="row">
    <div class="col-lg-6 col-sm-12">
      <!-- swiper start -->
      <div class="card bg-transparent shadow-none">
        <div class="card-content">
          <div class="card-body">
            <div class="swiper-centered-slides swiper-container p-1">
              <div class="swiper-wrapper">
                <div class="swiper-slide rounded swiper-shadow" id="getting-text"> <i
                  class="bx bxs-cart mb-1 font-large-1"></i>
                  <div class="cent-text1 master-font text-white">TRANSACTION HISTORY</div>
                </div>
                <div class="swiper-slide rounded swiper-shadow" id="sales-text"> <i
                  class="bx bx-movie mb-1 font-large-1"></i>
                  <div class="cent-text1 master-font text-white">BOOK TICKET</div>
                </div>
                <div class="swiper-slide rounded swiper-shadow" id="pricing-text"> <i
                  class="bx bx-dish mb-1 font-large-1"></i>
                  <div class="cent-text1 master-font text-white">FOOD & BEVERAGES</div>
                </div>
                <!-- <div class="swiper-slide rounded swiper-shadow" id="usage-text"> <i
                  class="bx bx-user-check mb-1 font-large-1"></i>
                  <div class="cent-text1 master-font text-white">Edit Profile</div>
                </div> -->
                <!--  <div class="swiper-slide rounded swiper-shadow" id="general-text"> <i
                  class="bx bx-user-pin mb-1 font-large-1"></i>
                  <div class="cent-text1 master-font text-white">Customer Service</div>
                </div> -->
              </div>
              <!-- Add Arrows -->
              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
            </div>
            <div class="main-wrapper-content">
              <div class="wrapper-content" data-faq="getting-text">
                <div class="text-center p-md-4 p-sm-1 py-1 p-0">
                  <h1 class="faq-title text-white master-font">Order History</h1>
                  <div class="card">
                    <div class="table-responsive" style="padding: 2rem;">
                      
                      <table id="example" class="display" style="width:100%">
                        <thead>
                          <tr>
                            <th>Booking ID</th>
                            <th></th>
                            <th>Park</th>
                            <th>Passenger</th>
                            <th>Total</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($booking as $item)
                          <tr>
                            <td>
                              <a href="{{url('dashboard/invoice/'.$item->booking_code)}}">#{{$item->booking_code}}</a>
                            </td>
                            <td></td>
                            <td>{{ $item->seat->name }}</td>
                            <td>{{ $item->qty }}</td>
                            <td>@curencyF($item->total)</td>
                            <td>{{$item->stat->name}}</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- accordion start -->
                <!-- Accordion end -->
              </div>
              <div class="wrapper-content" data-faq="pricing-text">
                <div class="text-center p-md-4 p-sm-1 py-1 p-0">
                  <h1 class="faq-title text-white">Coming Soon</h1>
                </div>
              </div>
              <div class="wrapper-content" data-faq="sales-text">
                <div class="text-center p-md-4 p-sm-1 py-1 p-0">
                  
                  <img src="https://cdn.portalquad.com/public/images/logo/logo-book.png" style="margin: auto" class="d-none d-sm-block img img-responsive">
                  <img src="https://cdn.portalquad.com/public/images/logo/logo-book.png" style="width: 100%" class="d-md-none d-lg-non d-sm-none img img-responsive"><br>
                  <!-- <h1 class="faq-title master-font text-white">DRIVE IN CINEMA</h1> -->
                  <a href="{{url('terms_condition')}}" class="text-center text-white" style="font-family: inherit;
                    text-transform: uppercase;
                    letter-spacing: 0.1rem;
                    margin-top: 8px;"> <small> terms & conditions privacy policy </small>
                  </a>
                  <button data-toggle="modal" data-target="#modal_question" class="btn btn-warning shadow button-test" style="margin: 5px;width: 80%;padding: 25px">
                  BOOK NOW !
                  </button>
                </div>
                <!-- accordion start -->
                <div id="accordion-icon-wrapper3" class="collapse-icon accordion-icon-rotate">
                </div>
                <!-- Accordion end -->
              </div>
              <div class="wrapper-content" data-faq="usage-text">
                <div class="row text-center p-md-4 p-sm-1 py-1 p-0">
                  <!-- <h1 class="faq-title master-font">Edit Your Profile</h1> -->
                  <div class="col-md-2 col-2 px-0"></div>
                  <div class="col-lg-8 col-sm-12 px-0">
                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                      <div class="card-header pb-1">
                        <div class="card-title">
                          <h4 class="text-center header">Update Your Data</h4>
                        </div>
                      </div>
                      <div class="text-center">
                        <p> <small> Please enter your details to sign up </small>
                        </p>
                      </div>
                      <div class="card-content">
                        <div class="">
                          <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group mb-50">
                              <!-- <label class="text-bold-600" for="name">Name</label> -->
                              <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ auth()->user()->name }}" required autocomplete="name" autofocus placeholder="Full Name">
                              @error('name')
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                            </div>
                            <div class="form-group mb-50">
                              <!-- <label class="text-bold-600" for="email">Email address</label> -->
                              <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ auth()->user()->email }}" required autocomplete="email" placeholder="Email address">
                              @error('email')
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                            </div>
                            <fieldset class="form-group">
                              <!-- <label class="text-bold-600" for="Car">Car Type</label> -->
                              <select class="custom-select" id="customSelect" name="car_type" required>
                                <option value="{{ auth()->user()->car_type }}">{{ auth()->user()->car_type }}</option>
                                <option value="SUV">SUV</option>
                                <option value="SEDAN">SEDAN</option>
                                <option value="TRUCK">TRUCK</option>
                              </select>
                            </fieldset>
                            <div class="form-group mb-2">
                              <!-- <label class="text-bold-600" for="password">Password</label> -->
                              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" value="{{ auth()->user()->password }}" name="password" required autocomplete="new-password" placeholder="Password">
                              @error('password')
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                              </span>
                              @enderror
                            </div>
                            <!-- <div class="form-group mb-2">
                              <label class="text-bold-600" for="password-confirm">Confirm Password</label>
                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
                            </div> -->
                            <button type="submit" class="btn btn-warning shadow position-relative w-100">UPDATE DATA<i
                            id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                          </form>
                          <hr>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- accordion start -->
                <div id="accordion-icon-wrapper4" class="collapse-icon accordion-icon-rotate">
                </div>
                <!-- Accordion end -->
              </div>
              <div class="wrapper-content" data-faq="general-text">
                <div class="row text-center p-md-4 p-sm-1 py-1 p-0">
                  <div class="col-md-2 col-2 px-0"></div>
                  <div class="col-lg-8 col-sm-12  px-0">
                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                      <div class="card-header pb-1">
                        <div class="card-title">
                          <h4 class="text-center header">Tell Your Detail</h4>
                        </div>
                      </div>
                      <div class="text-center">
                        <p> <small> Please enter your details</small>
                        </p>
                      </div>
                      <div class="card-content">
                        <div class="">
                          <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="col-12">
                              <fieldset class="form-group">
                                <textarea class="form-control" id="basicTextarea" rows="8" placeholder="Textarea"></textarea>
                              </fieldset>
                            </div>
                            <button type="submit" class="btn btn-warning shadow position-relative w-100">UPDATE DATA<i
                            id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                          </form>
                          <hr>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- accordion start -->
                <div id="accordion-icon-wrapper5" class="collapse-icon accordion-icon-rotate">
                </div>
                <!-- Accordion end -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- swiper ends -->
    </div>
    <div class="col-6 auto-center d-xl-block d-none" style="padding: 0;margin: 0;margin-top: 10vh">
      <img src="https://cdn.portalquad.com/public/images/logo/soon-logo.png" class="img img-responsive" style="width: 60%">
    </div>
  </div>
  <div class="col-md-6 col-xs-12 text-center">
    
    <img src="https://cdn.portalquad.com/public/images/logo/tools.png" style="width: 300px;margin-top: -10vh;" class="img img-responsive">
  </div>
  <div class="col-md-6"></div>
</section>
<!--modal-->
<form action="#" method="post">
  @csrf
  <div class="modal fade" id="modal_question" tabindex="-1" role="dialog"aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable"
      role="document">
      <div class="modal-content">
        <div class="modal-body" style="padding: 0 !important">
          <div class="ticket-card active">
            <!--    <div class="cover">
              <img src="" alt="" />
              <div class="info master-font">
              </div>
            </div> -->
            <div class="body">
              <h5 class="master-font">Are you going to watch Nobar Drive-in Cinema with your family*?</h5>
              <ul class="list-unstyled mb-0">
                <li class="d-inline-block mr-2 mb-1">
                  <fieldset>
                    <div class="checkbox">
                      <input type="checkbox" class="checkbox-input radio check_question" id="checkbox1" value="1" name="type_passenger" >
                      <label for="checkbox1">Yes, I am going to watch with my family.</label>
                    </div>
                  </fieldset>
                </li>
                <li class="d-inline-block mr-2 mb-1">
                  <fieldset>
                    <div class="checkbox">
                      <input type="checkbox" class="checkbox-input radio check_question" id="checkbox2" value="2" name="type_passenger" >
                      <label for="checkbox2">No, I am not going to watch with my family.</label>
                    </div>
                  </fieldset>
                </li>
              </ul>
              <div  style="padding: 0.5vw;">
                <p id="show-question" style="text-align: justify;font-family: serif;display: none"">
                  By choosing “Yes”, you agreed to follow the Regional Government of DKI Jakarta rules on Large-Scale Social Restrictions (PSBB).
                  Nobar Drive-in Cinema team will validate your ticket and Family Card at the venue. If the data provided is not valid, our team will have the rights to take a decisive action
                </p>
                <hr>
                
                <p class="text-right">*must under one Family Card (Kartu Keluarga).</p>
                <fieldset  id="checkbox_1" style="display: none">
                  <div class="checkbox">
                    <input type="checkbox" class="checkbox-input seat-submit"  id="submit" value="1" required>
                    <label for="submit">I agree with the <a href="{{url('terms_condition')}}">Terms and Conditions.</a> </label>
                  </div>
                </fieldset>
              </div>
            </div>
            <div class="footer actions">
              <div class="footer actions">
                <button type="button" id="button_add_seat" type="" data-toggle="modal" data-target="#choose_match" class="btn button-seat" disabled>S U B M I T</button>
              </div>
              <!--  <button type="submit" id="testcoki" type="" class="btn">S U B M I T</button> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--modal-select-match-->
  <div class="modal fade" id="choose_match" tabindex="-1" role="dialog" style="display: none;background: url(https://cdn.portalquad.com/public/images/logo/background-master.jpg);" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
      <div class="modal-content" style="background: #ffffff17;">
        <div class="modal-body" style="padding: 0 !important;background: none !important">
          <div class="ticket-card active" style="background: none !important">
            
            <div class="body" style="padding: 0px !Important;background: none !important">
              <div class="col-md-12">
            <h3 class="master-font text-white text-center">Select Match</h3>
                @foreach($project as $item_project)
                <div class="inputGroup" style="background: none !important;">
                  <input id="radio{{ $item_project->id }}" name="project" value="{{ $item_project->id }}" class="match-submit" type="radio"/>
                  <label for="radio{{ $item_project->id }}">
                    <div class="row">
                      <div class="col-4 text-center">
                        <img style="width: 80%" src="{{ $item_project->home }}" class="img img-responsive">
                      </div>
                      <div class="col-4 text-center">
                        <p class="text-center text-white" style="margin-top: 5vh">{{ $item_project->date }}<BR>{{ $item_project->time_start }}</p>
                      </div>
                      <div class="col-4 text-center">
                        <img style="width: 80%" src="{{ $item_project->away }}" class="img img-responsive">
                      </div>
                    </div>
                  </label>
                  
                </div>
                <hr>
                @endforeach
                <!-- <div class="inputGroup" style="background: none !important;">
                  <input id="radio2" name="payment" value="qris" class="match-submit" type="radio"/>
                  <label for="radio2">
                    <div class="row">
                      <div class="col-4 text-center">
                        <img style="width: 80%" src="https://upload.wikimedia.org/wikipedia/id/thumb/7/7a/Manchester_United_FC_crest.svg/1200px-Manchester_United_FC_crest.svg.png" class="img img-responsive">
                      </div>
                      <div class="col-4 text-center">
                        <p class="text-center text-white" style="margin-top: 5vh">20 JUNI 2020<BR>02:15</p>
                      </div>
                      <div class="col-4 text-center">
                        <img style="width: 80%" src="https://upload.wikimedia.org/wikipedia/id/thumb/7/7a/Manchester_United_FC_crest.svg/1200px-Manchester_United_FC_crest.svg.png" class="img img-responsive">
                      </div>
                    </div>
                  </label>
                  
                </div> -->

              </div>
            </div>
             <div class="footer actions">
                <button type="button" id="button_add_seat" type="" data-toggle="modal" data-target="#choose_seat" class="btn button-match" disabled>S U B M I T</button>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--modal-seatselect-->
  <div class="modal fade" id="choose_seat" tabindex="-1" role="dialog" style="display: none;background: url(https://cdn.portalquad.com/public/images/logo/background-master.jpg);" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-body" style="padding: 0 !important">
          <div class="ticket-card active">
            <div class="cover" style="margin-top: 10vh;">
              <img src="https://cdn.portalquad.com/public/images/logo/logo-book.png" alt="">
            </div>
            <div class="body">
              <div class="col-md-12">
                <label for="basicInput">Type Of Car</label>
                <div class="project_images">
                </div>
              </div>
              <div class="col-md-12">
                <fieldset class="form-group">
                  <label for="basicInput">License Plat Number</label>
                  <input type="text" class="form-control" id="basicInput" name="license_plat" placeholder="Enter License Plat Number" required>
                </fieldset>
              </div>
            </div>
            <div class="footer actions">
              <button type="submit" id="testcoki" type="submit" class="btn btn-warning">S U B M I T</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
<!--end modal-->
@endsection
{{-- vendor scripts --}}
@section('vendor-scripts')
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script src="https://cdn.portalquad.com/public/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/extensions/swiper.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
<!-- <script src="https://cdn.portalquad.com/public/vendors/js/tables/datatable/responsive.bootstrap.min.js"></script> -->
@endsection
{{-- page scripts --}}
@section('page-scripts')
<script src="https://cdn.portalquad.com/public/js/scripts/forms/wizard-steps.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/forms/validation/form-validation.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/pages/faq.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/forms/form-repeater.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/custom-ext.js?v=1.2.8"></script>
<!-- <script src="{{asset('vendors/js/custom-ext.js')}}"></script> -->
<script src="https://cdn.portalquad.com/public/js/scripts/pages/app-invoice.js"></script>
<script>
$(document).ready(function() {
$('#example').DataTable( {
"lengthMenu": [[3], [3]],
responsive: true
} );
} );
$("input:checkbox").on('click', function() {
// in the handler, 'this' refers to the box clicked on
var $box = $(this);
if ($box.is(":checked")) {
// the name of the box is retrieved using the .attr() method
// as it is assumed and expected to be immutable
var group = "input:checkbox[name='" + $box.attr("name") + "']";
// the checked state of the group/box on the other hand will change
// and the current value is retrieved using .prop() method
$(group).prop("checked", false);
$box.prop("checked", true);
} else {
$box.prop("checked", false);
}
});
$(document).ready(function(){
$('#checkbox1').change(function(){
if(this.checked){
$('#show-question').fadeIn('slow');
$('#checkbox_1').fadeIn('slow');
$(".project_images").append(
'<fieldset class="form-group project_family">'
  + '<select class="custom-select" id="submit_final" name="seat" required>'
    + '<option value="">Select Car Type</option>'
    + '<option value="2">2-Seaters Car (Max. 2 person)</option>'
    + '<option value="5">4-5 Seaters Car (Max. 5 person</option>'
    + '<option value="7">6-7 Seaters Car (Max. 7 person)</option>'
    + '<option value="8">8 Seaters Car or more (8 person or more)</option>'
  + '</select>'
+ '</fieldset>');
$(".project_non_family").remove();
}else{
$('#show-question').fadeOut('slow');
}
});
$('#checkbox2').change(function(){
if(this.checked){
$('#checkbox_1').fadeIn('slow');
$('#show-question').fadeOut('slow');
$(".project_images").append(
'<fieldset class="form-group project_non_family">'
  + '<select class="custom-select" id="submit_final" name="seat" required>'
    + '<option value="">Select Car Type</option>'
    + '<option value="1">2-Seaters Car (Max. 1 person)</option>'
    + '<option value="3">4-5 Seaters Car (Max. 3 person)</option>'
    + '<option value="4">6-7 Seaters Car (Max. 4 person)</option>'
    + '<option value="5">8 Seaters Car or more (Max. 5 person or 50% of Capacity)</option>'
  + '</select>'
+ '</fieldset>');
$(".project_family").remove();
}else{
}
});
});
$('.seat-submit').change(function()
{
if(this.checked){
$('.button-seat').removeAttr("disabled");
}else{
$(".button-seat").attr("disabled", true);
}
});
$('.match-submit').change(function()
{
if(this.checked){
$('.button-match').removeAttr("disabled");
}else{
$(".button-match").attr("disabled", true);
}


});
</script>
@endsection