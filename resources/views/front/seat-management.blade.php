@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title', 'templateTitle')
{{-- page style --}}
@section('vendor-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/custom-ext.css">
<!-- <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/custom-ext.css')}}"> -->
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/fancybox/jquery.fancybox.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/fancybox/core.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/extensions/swiper.min.css">
@endsection
@section('page-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/plugins/extensions/swiper.css">
<link rel="stylesheet" type="text/css" href="https://cdn.portalquad.com/public/vendors/css/pages/faq.css">

<style>


@media only screen and (min-device-width: 481px) and (max-device-width: 1800px) and (orientation:landscape) {

   div.theater {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 1.75rem 0;
	}
	div.theater p {
    text-align: center;
    text-transform: uppercase;
    padding: 0.5rem 3.4rem;
    color: hsl(0, 0%, 80%);
    border-radius: 20px;
    border: 1px solid currentColor;
    font-size: .6rem;
    letter-spacing: 0.1rem;
    background: #fff;
    position: relative;
}

div.theater p:before, div.theater p:after {
    position: absolute;
    content: "";
    top: 50%;
    transform: translate(0%, -50%);
    width: 5vw;
    height: 1px;
    background: currentColor;
}
.dashed {
    border-left: 1px dashed #000;
    margin: -1px;
}
.stock-images .image {
    opacity: 0.7;
    width: vw;
    height: 23vh;
    background-position: center center;
    background-color: gray;
    background-size: cover;
    margin-left: -3.4vh;
    width: 3vw;
}
.btn-warning {
    border-color: #fc960f !important;
    background-color: #FDAC41 !important;
    color: #fff;
    font-size: 1vw;
    width: 100% !important;
}
.pd-lands {
	padding: .5vw;
}

div.legend {
    display: contents !important;
}
}

</style>

@endsection
@section('content')


<div class="d-none d-sm-block">
    @include('include.layout-dekstop')
</div>
<div class="d-md-none d-lg-non d-sm-none">
    @include('include.layout-mobile')
</div>

@include('include.modal')
@endsection
{{-- vendor scripts --}}
@section('vendor-scripts')
<script src="https://cdn.portalquad.com/public/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jquery.validate.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/extensions/swiper.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="https://cdn.portalquad.com/public/vendors/js/fancybox/jquery.fancybox.min.js"></script>

@endsection
{{-- page scripts --}}
@section('page-scripts')
<script src="https://cdn.portalquad.com/public/js/scripts/forms/wizard-steps.js"></script>
<script src="https://cdn.portalquad.com/public/js/scripts/forms/validation/form-validation.js"></script>
<!-- <script src="{{asset('js/scripts/pages/faq.js')}}"></script> -->
<script src="https://cdn.portalquad.com/public/js/scripts/forms/form-repeater.js"></script>
  <!-- <script src="{{asset('vendors/js/custom-ext.js')}}"></script> -->

<script src="https://cdn.portalquad.com/public/vendors/js/custom-ext.js?v={{date('ymdHis')}}"></script>
<script>
// $(document).ready(function(){
// $('.mobile').hover(function(){
// 	// $("#demo-mobile").html(value_1);
// 	var show_park = $(this).find('.mobile-show-text').text();
// 	$("#demo-mobile").html(show_park);
	
// })
//     });
$(document).ready(function() {

    /* This is basic - uses default settings */
    
    $("a#single_image").fancybox();
    
    /* Using custom settings */
    
    $("a#inline").fancybox({
        'hideOnContentClick': true
    });

    /* Apply fancybox to multiple items */
    
    $("a.group").fancybox({
        'transitionIn'  :   'elastic',
        'transitionOut' :   'elastic',
        'speedIn'       :   600, 
        'speedOut'      :   200, 
        'overlayShow'   :   false
    });
    
});
</script>
@endsection
