@component('mail::message')

Dear {{$order->user->name}}

<p>Thank you for purchasing ticket</p>

<p>Detail Order:</p>
<div class="form-body">
    <div class="form-group">
        <p class="form-control-static text-mint">Order No: {{$order->booking_code}}</p>
    </div>
    <div class="form-group">
        <p class="form-control-static">Booking Date: {{$order->created_at->format('d F Y H:i')}}</p>
    </div>
    <div class="form-group">
        <p class="form-control-static">Section: {{$order->seat->name}}</p>
    </div>
    <div class="form-group">
        <p class="form-control-static">Qty: {{$order->qty}}</p>
    </div>
</div>


@component('mail::button', ['url' => url('dashboard/invoice/'.$order->booking_code)])
View Evoucher
@endcomponent

Thanks,<br>
Team
@endcomponent
