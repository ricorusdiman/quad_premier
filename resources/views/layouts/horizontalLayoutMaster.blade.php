<!-- BEGIN: Body-->
<body class="horizontal-layout horizontal-menu @if(isset($configData['navbarType']) && ($configData['navbarType'] !== "navbar-hidden") ){{$configData['navbarType']}} @else {{'navbar-sticky'}}@endif 2-columns
  @if($configData['theme'] === 'dark'){{'dark-layout'}} @elseif($configData['theme'] === 'semi-dark'){{'semi-dark-layout'}} @else {{'light-layout'}} @endif
  @if($configData['isContentSidebar']=== true) {{'content-left-sidebar'}} @endif
  @if(isset($configData['footerType'])) {{$configData['footerType']}} @endif {{$configData['bodyCustomClass']}}
  @if($configData['isCardShadow'] === false){{'no-card-shadow'}}@endif"
  data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
  <!-- BEGIN: Header-->
  @include('panels.horizontal-navbar')
  <!-- END: Header-->
  <!-- BEGIN: Main Menu-->
  @auth
  @include('panels.sidebar')
  @endauth
  @if(Route::current()->getName() != 'seat')
  <style>
  body{
  background: url(https://cdn.portalquad.com/public/images/logo/background-master.jpg);
  background-size: cover;
  }
  </style>
  @else
  <style>
    .horizontal-menu .header-navbar.navbar-brand-center .navbar-header .navbar-brand .brand-text {
    color: #102c42;
    padding-left: 1rem;
    letter-spacing: 0.01rem;
    font-size: 1.57rem;
}
  </style>
  @endif
  
  <style>
  .bg-rgba-warning.alert {
  color: #ffffff;
  }
  }
  .bg-rgba-warning {
  background: rgba(0, 0, 0, 0.87) !important;
  }
  </style>
  <!-- END: Main Menu-->
  <!-- BEGIN: Content-->
  <div class="app-content content">
    {{-- Application page structure --}}
    @if($configData['isContentSidebar'] === true)
    <div class="content-area-wrapper">
      <div class="sidebar-left">
        <div class="sidebar">
          @yield('sidebar-content')
        </div>
      </div>
      <div class="content-right">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
          <div class="content-header row">
          </div>
          <div class="content-body">
            @yield('content')
          </div>
        </div>
      </div>
    </div>
    @else
    {{-- others page structures --}}
    <div class="content-overlay"></div>
    
    <div class="content-wrapper">
      <div class="content-header row">
        @if($configData['pageHeader'] === true && isset($breadcrumbs))
        @include('panels.breadcrumbs')
        @endif
      </div>
      <div class="content-body">
        @yield('content')
      </div>
    </div>
    @endif
  </div>
  <!-- END: Content-->
  @auth
  @if($configData['isCustomizer'] === true && isset($configData['isCustomizer']))
  <!-- BEGIN: Customizer-->
  <!--   <div class="customizer d-none d-md-block">
    <a class="customizer-close" href="#"><i class="bx bx-x"></i></a>
    <a class="customizer-toggle" href="#"><i class="bx bx-cog bx bx-spin white"></i></a>
    
  </div> -->
  <!-- End: Customizer-->
  @endif
  @endauth
  <div class="sidenav-overlay"></div>
  <div class="drag-target"></div>
  <!-- BEGIN: Footer-->
  @include('panels.footer')
  <!-- END: Footer-->
  @include('panels.scripts')
</body>
<!-- END: Body-->