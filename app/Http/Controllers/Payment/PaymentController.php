<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Mail\Evoucher;
use App\Models\Booking\Hd;
use App\Models\StatusApi;
use Illuminate\Http\Request;
use Mail;

class PaymentController extends Controller
{
    public function result(Request $request)
    {
        $payLoad = json_decode(request()->getContent(),true);
        $testing = request()->getContent();
        //return $payLoad['bank'];
        //return $payLoad['payment_type'];
        $Voucher_booking = Hd::where('booking_code',$payLoad['booking_code'])->first();
        //return $Voucher_booking;
        //return  $payLoad['status_code'];
        //return $payLoad['booking_code'];
        //return $Voucher_booking;
        //return request()->getContent();
        $Voucher = StatusApi::where('order_id',$Voucher_booking->id)->first();
        //return $Vouccher;
        if (!$Voucher) {
        $Voucher = new StatusApi;
        $Voucher->order_id = $Voucher_booking->id;
        }

        $Voucher->status_code = $payLoad['status_code'];
        $Voucher->status_message = $payLoad['status_message'];
        if (isset($payLoad['approval_code'])) {
        $Voucher->approval_code = $payLoad['approval_code'];
        }

        if (isset($payLoad['masked_card'])) {
        $Voucher->masked_card = $payLoad['masked_card'];
        }
        if (isset($payLoad['bank'])) {
        $Voucher->bank = $payLoad['bank'];
        }
        if (isset($payLoad['transaction_status'])) {
        $Voucher->transaction_status = $payLoad['transaction_status'];
        }

        if (isset($payLoad['transaction_id'])) {
        $Voucher->transaction_id = $payLoad['transaction_id'];
        }

        if (isset($payLoad['transaction_time'])) {
        $Voucher->transaction_time = $payLoad['transaction_time'];
        }

        if (isset($payLoad['fraud_status'])) {
        $Voucher->fraud_status = $payLoad['fraud_status'];
        }

        if (isset($payLoad['channel_response_code'])) {
        $Voucher->channel_response_code = $payLoad['channel_response_code'];
        }

        if ($payLoad['status_code'] == 200) {
            if ($payLoad['transaction_status'] == 'settlement') {
                # code...

            $Voucher->status = 2;
            $Voucher_booking->status = 1;
            $Voucher_booking->seat->status = 3;
            $Voucher_booking->seat->save();
            $Voucher_booking->save();
            //return $Voucher_booking->user;
            Mail::to($Voucher_booking->user->email,$Voucher_booking->user->name)->send(new Evoucher($Voucher_booking));
            }elseif($payLoad['transaction_status'] == 'refund') {
                $Voucher->status = 2;
                $Voucher_booking->status = 2;
                $Voucher_booking->seat->status = 1;
                $Voucher_booking->seat->save();
                $Voucher_booking->save();
            }

        }elseif ($payLoad['status_code'] == 201) {
            $Voucher->status = 5;
            $Voucher_booking->status = 3;

        }elseif ($payLoad['status_code'] == 202) {
            $Voucher->status = 3;
            $Voucher_booking->status = 2;
            $Voucher_booking->seat->status = 1;
            $Voucher_booking->seat->save();
            $Voucher_booking->save();
        }
        else {
            $Voucher->status = 4;
            $Voucher_booking->status = 5;
        }
        //return  $Voucher->user;
        $Voucher->save();
        $Voucher_booking->save();


        //return $res_head;



        return $Voucher;
    }
}
