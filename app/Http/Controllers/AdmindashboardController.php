<?php

namespace App\Http\Controllers;

use App\Models\Booking\Dt;
use App\Models\Booking\Hd;
use App\Models\Project;
use App\Models\Seat;
use App\Models\StatusApi;
use Illuminate\Http\Request;
use App\Traits\Barcode;
use GuzzleHttp\Client;

class AdmindashboardController extends Controller
{
    use Barcode;

     public function signup(){

        return view('front.register');
    }


   public function order_view(Request $request){
      
        $pageConfigs = [
            'mainLayoutType'=>'horizontal-menu',
            'theme' => 'light',
            'navbarBgColor' => 'bg-centagram',
            'navbarType' => 'fixed',
            'footerType' => 'hidden',
            'templateTitle'=>'JAKARTA OPEN AIR FESTIVAL'
        ];
      
        $booking = Hd::all();
        $project = Project::where('status',1)->first();
        return view('front.admin.order-view', ['project'=>$project,'order'=>$booking,
        'pageConfigs' => $pageConfigs
        ]);
    }

       public function order_view_detail(Request $request, $booking_number){
      
        $pageConfigs = [
            'mainLayoutType'=>'horizontal-menu',
            'theme' => 'light',
            'navbarBgColor' => 'bg-centagram',
            'navbarType' => 'fixed',
            'footerType' => 'hidden',
            'templateTitle'=>'JAKARTA OPEN AIR FESTIVAL'
        ];
      
        $booking = Hd::where('booking_code','0MD3NHWIO8')->first();
        // return $booking;
        $project = Project::where('status',1)->first();
        return view('front.admin.order-view-detail', ['project'=>$project,'order'=>$booking,
        'pageConfigs' => $pageConfigs
        ]);
    }
}
