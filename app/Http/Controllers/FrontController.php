<?php

namespace App\Http\Controllers;

use App\Models\Booking\Dt;
use App\Models\Booking\Hd;
use App\Models\Project;
use App\Models\Seat;
use App\Models\StatusApi;
use Illuminate\Http\Request;
use App\Traits\Barcode;
use GuzzleHttp\Client;

class FrontController extends Controller
{
    use Barcode;

     public function signup(){

        return view('front.register');
    }


    public function seat_select(Request $request){
        $type_passenger = $request->a;
        $seat_number = $request->b;
        $license_plat = $request->c;
        $projects = $request->d;

         if(!$projects){
            abort(404);
        }
        //type_passenger_get
        if($type_passenger > 2 ){
            abort(404);
        }elseif(!$type_passenger){
            abort(404);
        }else{

        }

        //seat_get
        if(!$seat_number){
            abort(404);
        }



        //license_plat_get
        if(!$license_plat){
            abort(404);
        }
        $pageConfigs = [
            'mainLayoutType'=>'horizontal-menu',
            'theme' => 'light',
            'navbarBgColor' => 'bg-centagram',
            'navbarType' => 'fixed',
            'footerType' => 'hidden',
            'templateTitle'=>'JAKARTA OPEN AIR FESTIVAL'
        ];
        // $k = 12;
        // for ($i=0; $i < 11; $i++) {
        //     $seat = new Seat;
        //     $seat->project_id = 1;
        //     $seat->name = 'E '.$k;
        //     $seat->price = 0;
        //     $seat->status = 1;
        //     $seat->type = 1;
        //     $seat->save();
        //     $k++;
        // }
        $project = Project::where('id',$projects)->first();
     
        // $seat = Seat::whereHas('project',function($q){
        //     $q->where('status',1);
        // })->get();
        $seat = Seat::where('project_id', $project->id)->get();
       
        return view('front.seat-management', ['seat_number'=>$seat_number,'type_passenger'=>$type_passenger,'license_plat'=>$license_plat,'seat'=>$seat,'project'=>$project,
        'pageConfigs' => $pageConfigs
        ]);
    }

    public function type_post(Request $request){
        $type_passenger = $request->type_passenger;
        $seat = $request->seat;
        $license_plat = $request->license_plat;
        $project = $request->project;
        return redirect('dashboard/seat-select?a='.$type_passenger.'&b='.$seat.'&c='.$license_plat.'&d='.$project);
    }

    public function seat_select_post(Request $request){
        $barcode = $this->toket(10);
        // return request()->all();
        $project = Project::where('status',1)->first();
        $check_bookingannya = Hd::where('event_id',$project->id)->where('user_id',auth()->user()->id)->where('status',5)->first();

        if ($check_bookingannya) {
            //return $check_bookingannya;
            return redirect('dashboard/invoice/'.$check_bookingannya->booking_code);
        }
        // return request()->all();

        $find_order = Hd::where('booking_code',$barcode)->first();
        $check_random = false;
        while(!$check_random){
          if (!$find_order) {
            $barcode = $barcode;
            $check_random = true;
          }else {
            $barcode = $this->toket(5);
            $find_order = Hd::where('booking_code',$barcode)->first();
            $check_random = false;
          }
        }


        $qty = $request->name;
        $address = $request->address;
        $phone = $request->phone;
        $total_qty = count($qty);
        $price = $project->price;
        $total_price = $price*$total_qty;

        $k = 0;

        $seating = Seat::findOrFail($request->seating_id);

        if ($seating->status == 2) {
            return redirect('dashboard')->with('flash_error','ok');
        }elseif ($seating->status == 3) {
            return redirect('dashboard')->with('flash_error_b','ok');
        }

        //return $seating;

        //return $total_qty;
        $orderhd = New Hd;
        $orderhd->event_id = 1;
        $orderhd->pessanger_type = $request->pessanger_type;
        $orderhd->car_type = $request->seat_qty;
        $orderhd->licens = $request->license_plat;
        $orderhd->seat_id = $request->seating_id;
        $orderhd->user_id = auth()->user()->id;
        $orderhd->booking_code = $barcode;
        $orderhd->qty = $total_qty;

        $fee = 0;
        $totals = $fee+$total_price;
        $orderhd->fee = $fee;
        $orderhd->total = $totals;
        $orderhd->status = 5;
        $orderhd->payment_type = $request->payment;
        $orderhd->save();
        $count_qty = 0;
        foreach ($qty as $value) {
            //return $value;
            // if ($qty[$z] > 4) {
            //   return redirect()->back()->with('error_note','Sorry maximum capacity is 4');
            // }

            $barcodez = $this->toket(10);
            $find_orderz = Dt::where('barcode',$barcodez)->first();

            $check_randoms = false;
            while(!$check_randoms){
            if (!$find_orderz) {
                $barcodez = $barcodez;
                $check_randoms = true;
            }else {
                $barcodez = $this->toket(5);
                $find_orderz = Dt::where('barcode',$barcodez)->first();
                $check_randoms = false;
                }
            }

            $fee = 0;
            $total = $price+$fee;

            $orderdt = New Dt();
            $orderdt->order_id = $orderhd->id;
            $orderdt->name =  $value;
            $orderdt->qty = 1;
            $orderdt->phone = $phone[$count_qty];
            $orderdt->address = $address[$count_qty];
            $orderdt->price = $price;
            $orderdt->total = $total;
            $orderdt->barcode = $barcodez;
            $orderdt->save();
            $count_qty++;
          }

        $payment_type = $request->payment;

        $data_dt[] = ['id'=>$request->seating_id,'price'=>$totals/$total_qty,'quantity'=>$total_qty,'name'=>$request->park];

        if ($request->payment == 'qris') {
            $data = [
                "payment_type"=> 'gopay',
                "client_key"=> "QUADPREMIER",
                "server_key"=> "UVVBRFBSRU1JRVI=",
                "transaction_details"=> [
                    "order_id"=> $barcode,
                    "gross_amount"=> $orderhd->total
                ],
                "item_details"=> $data_dt,
                    "customer_details"=> [
                        "first_name"=> auth()->user()->name,
                        "last_name"=> " ",
                        "email"=> auth()->user()->email,
                        "phone"=> auth()->user()->phone_no,
                        "billing_address"=> [
                            "first_name"=> auth()->user()->name,
                            "last_name"=> " ",
                            "email"=> auth()->user()->email,
                            "phone"=> auth()->user()->phone,
                            "address"=> auth()->user()->address,
                            "city"=> 'Jakarta',
                            "postal_code"=> '123456',
                            "country_code"=> "IDN"
                        ],
                        "shipping_address"=> [
                            "first_name"=> auth()->user()->name,
                            "last_name"=> " ",
                            "email"=> auth()->user()->email,
                            "phone"=> auth()->user()->phone,
                            "address"=> auth()->user()->address,
                            "city"=> 'Jakarta',
                            "postal_code"=> '123456',
                            "country_code"=> "IDN"
                        ]
                    ]
                ];
        }elseif($request->payment =='bank_transfer'){
            $data = [
                "payment_type"=> 'bank_transfer',
                "client_key"=> "QUADPREMIER",
                "server_key"=> "UVVBRFBSRU1JRVI=",
                "transaction_details"=> [
                    "order_id"=> $barcode,
                    "gross_amount"=> $orderhd->total
                ],
                "item_details"=> $data_dt,
                "customer_details"=> [
                    "first_name"=> auth()->user()->name,
                    "last_name"=> " ",
                    "email"=> auth()->user()->email,
                    "phone"=> auth()->user()->phone_no,
                    "billing_address"=> [
                        "first_name"=> auth()->user()->name,
                        "last_name"=> " ",
                        "email"=> auth()->user()->email,
                        "phone"=> auth()->user()->phone,
                        "address"=> auth()->user()->address,
                        "city"=> 'Jakarta',
                        "postal_code"=> '123456',
                        "country_code"=> "IDN"
                    ],
                    "shipping_address"=> [
                        "first_name"=> auth()->user()->name,
                        "last_name"=> " ",
                        "email"=> auth()->user()->email,
                        "phone"=> auth()->user()->phone,
                        "address"=> auth()->user()->address,
                        "city"=> 'Jakarta',
                        "postal_code"=> '123456',
                        "country_code"=> "IDN"
                    ]
                ],
                "bank_transfer"=> [
                    "bank"=> "bni",
                    "va_number"=>auth()->user()->phone_no
                ]
            ];
        }
        //return $data;
        $url = env("PAYMENT_URL",'https://payment.dyandra-production.net/api/v1/send-json');

        //return $data;
          $client = new Client();
          $request = $client->post($url,[
                'headers'=> [
                  'content-type' => 'application/json',
                  'X-Client-key' => 'QUADPREMIER',
                  'X-Server-key' => 'UVVBRFBSRU1JRVI='
                ],
                'json' => $data,
                'http_errors' => true
          ]);
          //return $request->getBody();
          $res_head = $request->getHeader('Content-Type');
          $statuscode = $request->getStatusCode();

          if ($statuscode == 200) {
            $find = json_decode($request->getBody());
            if (isset($find->payment_type)) {
              // code...

                if ($find->payment_type == 'gopay') {
                    //  return $find->actions[2]->url;
                    $api_status = StatusApi::where('order_id',$orderhd->id)->first();

                    if (!$api_status) {
                        $api_status = New StatusApi;
                        $api_status->order_id = $orderhd->id;
                    }

                    $api_status->booking_code = $find->order_id;
                    $api_status->bank = $find->payment_type;
                    $api_status->transaction_id = $find->transaction_id;
                    $api_status->transaction_time = $find->transaction_time;
                    $api_status->transaction_status = $find->transaction_status;
                    $api_status->status_code = $find->status_code;
                    $api_status->response = $find->status_message;
                    $api_status->fraud_status = $find->fraud_status;
                    $api_status->qr_code = $find->actions[0]->url;
                    $api_status->deep_link  = $find->actions[1]->url;
                    $api_status->url_status = $find->actions[2]->url;
                    $api_status->status = 3;
                    $orderhd->status = 3;
                    $api_status->save();

                    $seating->status = 2;
                    $seating->save();
                    return  redirect('dashboard/invoice/'.$orderhd->booking_code);
                }elseif ($find->payment_type == 'bank_transfer') {
                    $api_status = StatusApi::where('order_id',$orderhd->id)->first();

                    if (!$api_status) {
                        $api_status = New StatusApi;
                        $api_status->order_id = $orderhd->id;
                    }

                    $api_status->booking_code = $find->order_id;
                    $api_status->bank = $find->va_numbers[0]->bank;
                    $api_status->transaction_id = $find->transaction_id;
                    $api_status->transaction_time = $find->transaction_time;
                    $api_status->transaction_status = $find->transaction_status;
                    $api_status->status_code = $find->status_code;
                    $api_status->response = $find->status_message;
                    $api_status->fraud_status = $find->fraud_status;
                    $api_status->qr_code = $find->va_numbers[0]->va_number;
                    $api_status->status = 3;
                    $orderhd->status = 3;
                    $api_status->save();

                    $seating->status = 2;
                    $seating->save();
                    return  redirect('dashboard/invoice/'.$orderhd->booking_code);
                }
            }else {
              $find = json_decode($request->getBody());
              return $request->getBody();
              $api_status = StatusApi::where('order_id',$orderhd->id)->first();
              if (!$api_status) {
                $api_status = New StatusApi;
                $api_status->order_id = $orderhd->id;
              }
              $api_status->status_code = $find->status_code;
              $api_status->response = $find->status_message;
              $api_status->status = 3;
              $orderhd->status = 3;
              $orderhd->save();
              $api_status->save();

              return  redirect('order/preview/?orderid='.$orderhd->booking_code);
            }
          }
        // return view('front.seat-management', [
        // 'pageConfigs' => $pageConfigs
        // ]);
    }



    // analystic
    public function dashboardAnalytics(){
        return view('pages.dashboard-analytics');
    }
}
