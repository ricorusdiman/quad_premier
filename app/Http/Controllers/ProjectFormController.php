<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class ProjectFormController extends Controller
{
  //Register uSERS
  public function projectForm_hd(){

    $pageConfigs = [
      'mainLayoutType'=>'horizontal-menu',
      'theme' => 'light',
      'navbarBgColor' => 'bg-centagram',
      'navbarType' => 'fixed',
      'footerType' => 'hidden',
      'templateTitle'=>'CENTAGRAM'
    ];
     return view('pages.centagram.centagram-form-project-hd',['pageConfigs'=>$pageConfigs]);
   }

   public function projectForm_hd_store(Request $request){

    $check_email = User::where('email', $request->email)->first();

    if($check_email){
      return redirect()->back()->with('message','Your Email Already Exist');
    }

    $user = New User;
    $user->first_name = $request->first_name;
    $user->last_name = $request->last_name;
    $user->email = $request->email;
    $user->user_identity = $request->id_user;
    $user->password = Hash::make($request->password);
    $user->secret = $request->password;
    $user->role = 1;
    $user->save();

    return redirect()->intended('/dashboard');
   }
     // END Register uSERS

    public function projectForm_dt($project_form){

    $pageConfigs = [
      'mainLayoutType'=>'horizontal-menu',
      'theme' => 'light',
      'navbarBgColor' => 'bg-centagram',
      'navbarType' => 'fixed',
      'footerType' => 'hidden',
      'templateTitle'=>'CENTAGRAM'
    ];

    return view('pages.centagram.centagram-form-project',['pageConfigs'=>$pageConfigs]);
  }
 
}
