<?php

namespace App\Http\Controllers;

use App\Models\Booking\Hd;
use App\Models\StatusApi;
use App\Models\Project;
use Illuminate\Http\Request;
use DB;

class FrontdashController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }


    public function index(){
        $pageConfigs = [
            'mainLayoutType'=>'horizontal-menu',
            'theme' => 'light',
            'navbarBgColor' => 'bg-centagram',
            'navbarType' => 'fixed',
            'footerType' => 'hidden',
            'templateTitle'=>'JAKARTA OPEN AIR FESTIVAL'
        ];
        $booking = Hd::where('user_id',auth()->user()->id)->get();
        $project = Project::all();  
        return view('front.index', ['booking'=>$booking,
        'pageConfigs' => $pageConfigs,'project'=>$project
        ]);
    }

        public function about_us(){
        $pageConfigs = [
            'mainLayoutType'=>'horizontal-menu',
            'theme' => 'light',
            'navbarBgColor' => 'bg-centagram',
            'navbarType' => 'fixed',
            'footerType' => 'hidden',
            'templateTitle'=>'JAKARTA OPEN AIR FESTIVAL'
        ];

        return view('front.about-us', [
        'pageConfigs' => $pageConfigs
        ]);
    }

   public function profile(){
        $pageConfigs = [
            'mainLayoutType'=>'horizontal-menu',
            'theme' => 'light',
            'navbarBgColor' => 'bg-centagram',
            'navbarType' => 'fixed',
            'footerType' => 'hidden',
            'templateTitle'=>'JAKARTA OPEN AIR FESTIVAL'
        ];

        return view('front.profile', [
        'pageConfigs' => $pageConfigs
        ]);
    }

     public function contact(){
        $pageConfigs = [
            'mainLayoutType'=>'horizontal-menu',
            'theme' => 'light',
            'navbarBgColor' => 'bg-centagram',
            'navbarType' => 'fixed',
            'footerType' => 'hidden',
            'templateTitle'=>'JAKARTA OPEN AIR FESTIVAL'
        ];

        return view('front.contact', [
        'pageConfigs' => $pageConfigs
        ]);
    }

    public function terms_condition(){
        $pageConfigs = [
            'mainLayoutType'=>'horizontal-menu',
            'theme' => 'light',
            'navbarBgColor' => 'bg-centagram',
            'navbarType' => 'fixed',
            'footerType' => 'hidden',
            'templateTitle'=>'JAKARTA OPEN AIR FESTIVAL'
        ];

        return view('front.terms-condition', [
        'pageConfigs' => $pageConfigs
        ]);
    }

    public function invoice_index_id($invoice_id){
        $order = Hd::where('user_id',auth()->user()->id)->where('booking_code',$invoice_id)->first();
        $pageConfigs = [
            'mainLayoutType'=>'horizontal-menu',
            'theme' => 'light',
            'navbarBgColor' => 'bg-centagram',
            'navbarType' => 'fixed',
            'footerType' => 'hidden',
            'templateTitle'=>'JAKARTA OPEN AIR FESTIVAL'
        ];
        //$k = StatusApi::where('order_id',$order->id)->first();
        //return $k;
        return view('front.invoice', ['order'=>$order,
        'pageConfigs' => $pageConfigs
        ]);
    }

}
