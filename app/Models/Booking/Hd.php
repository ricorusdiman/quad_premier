<?php

namespace App\Models\Booking;

use Illuminate\Database\Eloquent\Model;

class Hd extends Model
{
    protected $table = 'bookingHd';

    public function StatApis()
    {
        return $this->belongsTo('App\Models\StatusApi','id','order_id');
    }

    public function seat()
    {
        return $this->belongsTo('App\Models\Seat','seat_id');
    }

    public function stat(){
        return $this->belongsTo('App\Models\Status','status','status')->where('type',1);
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
