<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seat extends Model
{
    protected $table = 'seating';

    public function project()
    {
        return $this->belongsTo('App\Models\Project','project_id');
    }
}
